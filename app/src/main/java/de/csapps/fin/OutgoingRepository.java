package de.csapps.fin;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;

import java.util.List;

public class OutgoingRepository {
    private OutgoingDao outgoingDao;
    private LiveData<List<Outgoing>> allOutgoings;

    public OutgoingRepository(Application application) {
        OutgoingDatabase database = OutgoingDatabase.getInstance(application);
        outgoingDao = database.outgoingDao();
        allOutgoings = outgoingDao.getAllOutgoings();
    }

    // Non LiveData methods can't be executed in the main thread
    public void insert(Outgoing outgoing) {
        new InsertOutgoingAsyncTask(outgoingDao).execute(outgoing);
    }

    public void update(Outgoing outgoing) {
        new UpdateOutgoingAsyncTask(outgoingDao).execute(outgoing);
    }

    public void delete(Outgoing outgoing) {
        new DeleteOutgoingAsyncTask(outgoingDao).execute(outgoing);
    }

    public void deleteAllOutgoings() {
        new DeleteAllOutgoingAsyncTask(outgoingDao).execute();
    }

    // LiveData is automatically done in the background thread
    public LiveData<List<Outgoing>> getAllOutgoings() {
        return allOutgoings;
    }

    // AsyncTask to execute the database operations
    private static class InsertOutgoingAsyncTask extends AsyncTask<Outgoing, Void, Void> {
        private OutgoingDao outgoingDao;

        private InsertOutgoingAsyncTask(OutgoingDao outgoingDao) {
            this.outgoingDao = outgoingDao;
        }

        @Override
        protected Void doInBackground(Outgoing... outgoings) {
            outgoingDao.insert(outgoings[0]);
            return null;
        }
    }

    private static class UpdateOutgoingAsyncTask extends AsyncTask<Outgoing, Void, Void> {
        private OutgoingDao outgoingDao;

        private UpdateOutgoingAsyncTask(OutgoingDao outgoingDao) {
            this.outgoingDao = outgoingDao;
        }

        @Override
        protected Void doInBackground(Outgoing... outgoings) {
            outgoingDao.update(outgoings[0]);
            return null;
        }
    }

    private static class DeleteOutgoingAsyncTask extends AsyncTask<Outgoing, Void, Void> {
        private OutgoingDao outgoingDao;

        private DeleteOutgoingAsyncTask(OutgoingDao outgoingDao) {
            this.outgoingDao = outgoingDao;
        }

        @Override
        protected Void doInBackground(Outgoing... outgoings) {
            outgoingDao.delete(outgoings[0]);
            return null;
        }
    }

    private static class DeleteAllOutgoingAsyncTask extends AsyncTask<Void, Void, Void> {
        private OutgoingDao outgoingDao;

        private DeleteAllOutgoingAsyncTask(OutgoingDao outgoingDao) {
            this.outgoingDao = outgoingDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            outgoingDao.deleteAllOutgoings();
            return null;
        }
    }

}
