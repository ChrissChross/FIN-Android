package de.csapps.fin;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

@Dao
public interface OutgoingCategoryDao {

    @Insert
    void insert();

    @Update
    void update();

    @Delete
    void delete();


}
