package de.csapps.fin;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(
    entities = {
        Outgoing.class,
        OutgoingCategory.class,
        OutgoingItem.class
    },
    version = 1
)
public abstract class OutgoingDatabase extends RoomDatabase {

    private static OutgoingDatabase instance;

    public abstract OutgoingDao outgoingDao();
    public abstract OutgoingCategoryDao outgoingCategoryDao();
    public abstract OutgoingItemDao outgoingItemDao();

    public static synchronized OutgoingDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                context.getApplicationContext(),
                OutgoingDatabase.class, "outgoing_database"
            )
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
