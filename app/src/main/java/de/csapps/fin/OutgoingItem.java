package de.csapps.fin;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.NO_ACTION;

@Entity(
    tableName = "T02010_OUTGOING_ITEMS",
    foreignKeys = {
        @ForeignKey (
            entity = OutgoingCategory.class,
            parentColumns = "C01000_OUTGOING_CATEGORYID",
            childColumns = "C02010_OUTGOING_CATEGORYID",
            onDelete = NO_ACTION
        ), @ForeignKey (
            entity = Outgoing.class,
            parentColumns = "C02000_OUTGOINGID",
            childColumns = "C02010_OUTGOINGID",
            onDelete = CASCADE
        )
    })
public class OutgoingItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "C02010_OUTGOING_ITEMID")
    private int id;

    @ColumnInfo(name = "C02010_OUTGOING_CATEGORYID")
    private int outgoingCategoryId;

    @ColumnInfo(name = "C02010_OUTGOINGID")
    private int outgoingId;

    @ColumnInfo(name = "C02010_TITLE")
    private String title;

    @ColumnInfo(name = "C02010_DATE")
    private Date date;

    @ColumnInfo(name = "C02010_AMOUNT")
    private double amount;

    public OutgoingItem(String title, Date date, double amount) {
        this.title = title;
        this.date = date;
        this.amount = amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }
}
