package de.csapps.fin;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface OutgoingDao {

    @Insert
    void insert(Outgoing outgoing);

    @Update
    void update(Outgoing outgoing);

    @Delete
    void delete(Outgoing outgoing);

    @Query("DELETE FROM T02000_OUTGOINGS")
    void deleteAllOutgoings();

    @Query("SELECT * FROM T02000_OUTGOINGS ORDER BY C02000_DATE DESC")
    LiveData<List<Outgoing>> getAllOutgoings();
}
