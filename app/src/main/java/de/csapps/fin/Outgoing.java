package de.csapps.fin;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "T02000_OUTGOINGS")
public class Outgoing {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "C02000_OUTGOINGID")
    private int id;

    @ColumnInfo(name = "C02000_DATE")
    private Date date;

    public Outgoing(Date date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }
}
