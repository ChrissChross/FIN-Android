package de.csapps.fin;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "T01000_OUTGOING_CATEGORIES")
public class OutgoingCategory {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "C01000_OUTGOING_CATEGORYID")
    private int id;

    @ColumnInfo(name = "C01000_TITLE")
    private String title;

    public OutgoingCategory(String title) {
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
}
